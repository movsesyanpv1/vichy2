module func
    use const
    implicit none
contains
    subroutine solve(x,y,z,h,A)

        real(mp), dimension(0:) :: x, y, z, h
        real(mp) :: A
        integer :: i

        do i = 0, size(x, dim=1)
            x(i) = sum(h(0:i))
        enddo

    end subroutine solve

    function itsolve(z,y,d,e,f,g,x,h,c)
        real(mp) :: z,y,d,e,f,g,x,h,z1,c
        real(mp) :: itsolve

        z1 = 1
        itsolve = 0
        do while (abs(z1-itsolve).ge.eps)
            z1 = itsolve
            itsolve = d * z - e * y - f/g * sqrt((x+theta*h)**2 + 1) - &
            h/(1+A*h*theta*c)*(1-1/((z*(1-theta)+theta*z1+1)))
            !h/(1+A*h*theta*c)*(z*(1-theta)+theta*z1)/((z*(1-theta)+theta*z1+1))
        enddo
        !print*, abs(z1-itsolve)
        
    end function itsolve
end module func