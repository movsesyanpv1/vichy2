use func
use const

implicit none

real(mp), allocatable, dimension(:) :: x, y, z, h, l, b, c, d, e, f, g
real(mp) :: hc
integer :: i

!read(*,*) A
!read(*,*) n
hc = 1.0/n

if((abs(A).ge.100))then 
    allocate(x(0:2*n-1),y(0:2*n-1),z(0:2*n-1),h(0:2*n-1),l(0:2*n-1),b(0:2*n-1),c(0:2*n-1),d(0:2*n-1),e(0:2*n-1)&
        ,f(0:2*n-1),g(0:2*n-1))
    y(0) = 3
    z(0) = -1
    h = hc
    print*, h(1)
    l = (1 + 0.5*A*h(1)*(1-theta)) / (1 - 0.5*A*h(1)*theta)
    b = h(1) * (1-theta) / (1 - 0.5*A*h(1)*theta)
    c = -h(1) * theta / (1 - 0.5*A*h(1)*theta)
    d = (1+h(1)*(1-theta) + A*h(1)*theta*b) / (1 - h(1)*theta + A*h(1)*theta*c)
    e = h(1)*A*((1-theta)+theta*l) / (1-h(1)*theta+A*h(1)*theta*c)
    f = A*h(1)**2*theta / (1-h(1)*theta+A*h(1)*theta*c)
    g = 1 - 0.5*A*h(1)*theta
    h(0:n) = 10.0/abs(A)/n
    print*, h(1)
    l(0:n) = (1 + 0.5*A*h(1)*(1-theta)) / (1 - 0.5*A*h(1)*theta)
    b(0:n) = h(1) * (1-theta) / (1 - 0.5*A*h(1)*theta)
    c(0:n) = -h(1) * theta / (1 - 0.5*A*h(1)*theta)
    d(0:n) = (1+h(1)*(1-theta) + A*h(1)*theta*b) / (1 - h(1)*theta + A*h(1)*theta*c)
    e(0:n) = h(1)*A*((1-theta)+theta*l) / (1-h(1)*theta+A*h(1)*theta*c)
    f(0:n) = A*h(1)**2*theta / (1-h(1)*theta+A*h(1)*theta*c)
    g(0:n) = 1 - 0.5*A*h(1)*theta
    h(0) = 0
else
    allocate(x(0:n-1),y(0:n-1),z(0:n-1),h(0:n-1),l(0:n-1),b(0:n-1),c(0:n-1),d(0:n-1),e(0:n-1),f(0:n-1),g(0:n-1))
    y(0) = 3
    z(0) = -1
    h = hc
    l = (1 + 0.5*A*h(1)*(1-theta)) / (1 - 0.5*A*h(1)*theta)
    b = h(1) * (1-theta) / (1 - 0.5*A*h(1)*theta)
    c = -h(1) * theta / (1 - 0.5*A*h(1)*theta)
    d = (1+h(1)*(1-theta) + A*h(1)*theta*b) / (1 - h(1)*theta + A*h(1)*theta*c)
    e = h(1)*A*((1-theta)+theta*l) / (1-h(1)*theta+A*h(1)*theta*c)
    f = A*h(1)**2*theta / (1-h(1)*theta+A*h(1)*theta*c)
    g = 1 - 0.5*A*h(1)*theta
    h(0) = 0
endif

do i = 0, size(x, dim=1)-1
    x(i) = sum(h(0:i))
enddo

do i = 1,  size(x, dim=1)-1
    z(i) = itsolve(z(i-1),y(i-1),d(i),e(i),f(i),g(i),x(i-1),h(i),c(i))
    y(i) = l(i) * y(i-1) - b(i) * z(i-1) + c(i) * z(i) + h(i) * sqrt((x(i-1)+theta*h(i))**2+1)/g(i)
enddo

open(10,file="res.dat")
do i = 0, size(x, dim=1)-1
    write(10,*) x(i), y(i), z(i), l(i),b(i),c(i),d(i),e(i),f(i),g(i)
enddo
close(10)

deallocate(x,y,z,h,l,b,c,d,e,f,g)

end