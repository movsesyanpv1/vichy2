plot: run
	rm prog *.*o* ; gnuplot gr.plt
run: comp
	./prog
comp: module prog.f90
	gfortran prog.f90 *_mod.o -o prog -fopenmp
module: *_mod.f90
	gfortran -c *mod.f90